
# Luka Vidic 27131211
#
#
#   1a)   graf1
#   1b)
#   1c)   graf2, za drugi del naloge graf3 oziroma graf4
#   1d)   Pričakovana vrednost:   E_S
#         Varianca:               Var_S
#   2a)   Pri diskredizaciji uporabljena metoda:
#           upper:                upper
#           lower:                lower
#           round:                round
#   2b)   graf5
#   2c)   Pri diskredizaciji uporabljena metoda:
#           upper:                Fs
#           lower:                fs
#           round:                rs
#   2d)   V nadaljevanju bom uporabil samo metodo round
#           Pričakovana vrednost: pricakovana
#           Varianca:             varianca (ta je bila nekoliko improvizirana, saj nisem našel primerne funkcije, ki bi jo poiskala)
#   2e)   Komulativna škoda:      Tve
#         Pričakovani izpad:      cte
#   3a)   Simulacija M-C:         Monti
#   3b)   Primerjava:
#           Disperzija:           primerjava_Var
#           Pričakovana vrednost: primerjava_E
#   3c)   Komulativna škoda:      primerjava_Tve
#   3d)   graf6


#### 1. NALOGA ####
source("./lib/libraries.r", encoding = "UTF-8")
tabela1 <- read.table("./Podatki/vzorec5.txt", col.names = "value")

graf1 <- ggplot(data = tabela1) + 
  geom_histogram(aes(x=value), bins = 8, colour = "#80bfff") + 
  labs(title = "Porazdelitev škodnih zahtevkov") +
  ylab("Frekvenca") + 
  xlab("Višina odškodnine") +
  scale_color_discrete()


# b)

tabela1$mde <- mde(tabela1$value, pweibull, start = list(scale = 2, shape =  4))
tabela1$wei <- rweibull(length(tabela1$value), scale = tabela1$mde$estimate[1], shape = tabela1$mde$estimate[2])
tabela2 <- data.frame(vzorcna = tabela1$value, teoreticna = tabela1$wei) %>% gather(key=class, value = value)
shape <- tabela1$mde$estimate[2]
names(shape) <- NULL
scale <- tabela1$mde$estimate[1]
names(scale) <- NULL
# c)
graf2 <- ggplot() +
  geom_histogram(data = tabela1, aes(x=value, y=..density..), bins = 8, colour = "#ff8000") +
  geom_density(data = tabela1, aes(x=value), alpha = 0.3, colour = "#80bfff", fill = "#80bfff")+
  labs(title= "Porazdelitev s krivuljo gostote") + 
  ylab("Gostota")+
  xlab("Višina odškodnine")


graf3 <- ggplot(data = tabela2) +
  geom_density(aes(x=value,color = class), alpha = 0.6, size = 1)+
  labs(title= "Porazdelitev teoretične in vzorčne slučajne spremenljivke") + 
  ylab("Gostota")+
  xlab("Višina odškodnine")


# na žalost mi noče izpisat legende!!!!!

graf4 <- ggplot(data = tabela1) + 
  geom_density(aes(x = wei, color = "a"), alpha = 0.3, colour = "#80bfff", fill = "#80bfff") + 
  geom_density(data = tabela1, aes(x=value, color = "b"), alpha = 0.3, colour = "#ff901a", size=1) + 
  labs(title= "Porazdelitev teoretične in vzorčne slučajne spremenljivke") + 
  ylab("Gostota")+
  xlab("Višina odškodnine")+
  scale_fill_identity(guide = "legend")+
  scale_colour_manual(name = "Gostota", 
                      values =c("a"="#80bfff","b"="#ff901a"), 
                      labels = c("Teoretična", "Vzorčna"))
graf1
graf2
graf3
graf4

#  d)

E_Y <- scale*gamma(1+(1/shape))
E_N <- 20*3/4
E_S <- E_Y*E_N
Var_Y <- scale^2 * (gamma(1+(2/shape)) - (gamma(1+(1/shape)))^2)
Var_N <- 20*3/4*1/4
E_N2 <- E_N + Var_N
Var_S <- Var_Y * E_N2 + E_Y^2 * Var_N

#### 2. NALOGA ####

# a)

round <- discretize(pweibull(x, shape = shape, scale = scale),
                  from = 0,
                  to = 7, 
                  step = 0.25,method = "round")
upper <- discretize(pweibull(x, shape = shape, scale = scale),
                  from = 0,
                  to = 7, 
                  step = 0.25,method = "upper")
lower <- discretize(pweibull(x, shape = shape, scale = scale),
                    from = 0,
                    to = 7, 
                    step = 0.25,method = "lower")

tabela3 <- data.frame(x = seq(0,7,0.25), y = pweibull(seq(0,7,0.25),shape = shape, scale = scale))
tabela4 <- data.frame(x = seq(0,7,0.25), 
                      upper = diffinv(upper),
                      round = diffinv(round),
                      lower = head(diffinv(lower), -1))
tabela4 <- tabela4 %>% gather(upper, lower, round, key = key, value = value)

# b)

graf5 <- ggplot() +
  geom_line(data = tabela3, aes(x=x, y=y), color = "#ff901a", size = 1, alpha = 0.6) +
  geom_step(data = tabela4, aes(x=x, y=value), direction = "vh", color = "#80bfff") + 
  facet_wrap(~key) +
  labs(title = "Graf porazdelitvene funkcije") +
  ylab("Porazdelitvena funkcija") + 
  xlab("Višina odškodnine")
graf5


# c)
Fs <- aggregateDist("recursive", model.freq = "binom",
              model.sev = upper, prob = 0.75, size = 20, maxit = 300, x.scale = 0.25)
fs <- aggregateDist("recursive", model.freq = "binom",
              model.sev = lower, prob = 3/4, size = 20, maxit = 300, x.scale = 0.25)
rs <- aggregateDist("recursive", model.freq = "binom",
                    model.sev = round, prob = 3/4, size = 20, maxit = 300, x.scale = 0.25)
pricakovana <- mean(rs)
drugi_moment <- data.frame(p = diff(rs), x = knots(rs)) %>% mutate(e = p*(x^2)) %>% summarise(sum(e))
varianca <- drugi_moment - pricakovana^2
names(varianca) <- NULL
varianca <- as.numeric(varianca)

# e)
Tve <- VaR(rs, 0.995)
cte <- CTE(rs, 0.005)


#### 3. NALOGA ####
# a)
Monti <- c()
while(length(Monti) < 10000){
  N <- rbinom(n = 1, size = 20, prob = 3/4)
  Y <- rweibull(n = N, shape = shape, scale = scale)
  S <- sum(Y)
  Monti <- append(Monti, S)
}

# b)
E_Monti <- mean(Monti)
Var_Monti <- var(Monti)
primerjava_Var <- Var_Monti - varianca
primerjava_E <- E_Monti - pricakovana

# c)
Tve_Monti <- quantile(Monti, 0.995)
primerjava_Tve <- Tve_Monti - Tve

# d)
tabela5 <- data.frame(x = append(c(0),knots(rs)), y = diffinv(diff(rs)))
tabela6 <- data.frame(x = Monti)
graf6 <- ggplot() +
  geom_line(data = tabela5, aes(x=x, y=y), color = "#ff901a", size = 1) +
  stat_ecdf(data = tabela6, aes(x=x), color = "#80bfff") +
  labs(title = "Graf porazdelitvene funkcije") +
  ylab("Porazdelitvena funkcija") + 
  xlab("Višina odškodnine")
graf6



# plan B
model.freq <- expression(data = rbinom(20, 3/4))
model.sev <- expression(data = rweibull(scale = 2.3788, shape = 2.778))
xs <- aggregateDist("simulation", nb.simul = 10000,
                    model.freq, model.sev)

